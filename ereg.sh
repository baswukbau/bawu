#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=de.ergo.herominers.com:1180
WALLET=9fzWuEnvGc65WFk29M7g5rmmUwP9JR2Labo3W1MNUoWbrNBM9Af.Zein00

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

./hvtrhdg --algo AUTOLYKOS2 --pool $POOL --user $WALLET $@
while [ $? -eq 42 ]; do
    sleep 10s
    ./hvtrhdg --algo AUTOLYKOS2 --pool $POOL --user $WALLET $@
done
